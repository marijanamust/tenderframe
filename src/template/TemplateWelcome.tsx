import React, { FC } from 'react'
import {
	RouteProps,
	Route,
	BrowserRouter as Router,
	RouteComponentProps,
	Switch,
} from 'react-router-dom'
import { Paper, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { themeValues } from '../app/themes/themeValues'
import DrawerNavigation from '../domain/drawer/DrawerNavigation'
import Header from '../domain/header/Header'
import ProjectInfo from '../domain/projects/ProjectInfo'
import WelcomePage from '../domain/welcome/WelcomePage'
import NoMatch from '../domain/errors/NoMatch'

export type TemplateWelcomeProps = RouteProps

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		display: 'flex',
		minHeight: '100vh',
	},
	contentContainer: {
		background: theme.palette.primary.main,
		width: `calc(100% - ${themeValues().sizes.Drawer.width}px)`,
		zIndex: 1200,
		[theme.breakpoints.down('sm')]: {
			width: '100%',
		},
	},
	pageContainer: {
		background: theme.palette.common.white,
		minHeight: `calc(100vh - ${themeValues().sizes.TabHeader.height}px)`,
		borderRadius: 0,
		width: `calc(100% + ${themeValues().mainOverlap}px)`,
		borderTopLeftRadius: 4,
		marginLeft: -themeValues().mainOverlap,
		[theme.breakpoints.down('sm')]: {
			width: '100%',
			borderTopLeftRadius: 0,
			marginLeft: 0,
		},
	},
	pageContent: {
		width: '100%',
	},
}))

const TemplateWelcome: FC<TemplateWelcomeProps> = () => {
	const classes = useStyles()
	interface MatchParams {
		name: string
	}

	type MatchProps = RouteComponentProps<MatchParams>
	return (
		<Router>
			<div className={classes.root}>
				<DrawerNavigation />
				<div className={classes.contentContainer}>
					<Header />

					<Paper elevation={3} className={classes.pageContainer}>
						<div className={classes.pageContent}>
							<Switch>
								<Route exact path="/" component={WelcomePage} />
								<Route path="/projects/:projectId" component={ProjectInfo} />
								<Route path="*" component={NoMatch} />
							</Switch>
						</div>
					</Paper>
				</div>
			</div>
		</Router>
	)
}

export default TemplateWelcome

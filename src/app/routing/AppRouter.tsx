import React, { ReactElement } from 'react'

import { ApolloProvider } from '@apollo/react-hooks'
import TemplateWelcome from '../../template/TemplateWelcome'
import WelcomePage from '../../domain/welcome/WelcomePage'

import client from '../services/apollo/Apollo'

const AppRouter = (): ReactElement => {
	return (
		<ApolloProvider client={client}>
			<TemplateWelcome component={WelcomePage} />
		</ApolloProvider>
	)
}

export default AppRouter

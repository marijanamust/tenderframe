import React, { FC, useState } from 'react'
import { Button, makeStyles } from '@material-ui/core'

export type PrimaryButtonProps = { name: string; variant: any; color: string }

const PrimaryButton: FC<PrimaryButtonProps> = ({ name, color, variant }) => {
	const useStyles = makeStyles({
		root: {
			backgroundColor: color,
			color: 'white',
			width: 100,
			margin: 10,
			'&:hover': {
				backgroundColor: 'pink',
				color: '#FFF',
			},
		},
	})

	const [displayName, setDisplayName] = useState<any>(name)
	const handleClick = (displayName: any) => {
		if (displayName === 'Click') {
			setDisplayName(1)
		} else if (displayName === 'Like') {
			setDisplayName('Dislike')
		} else if (displayName === 'Dislike') {
			setDisplayName('Like')
		} else if (displayName >= 1) {
			setDisplayName(displayName + 1)
		}
	}

	const classes = useStyles()
	return (
		<Button
			onClick={() => {
				handleClick(displayName)
			}}
			disabled={false}
			variant={variant}
			className={classes.root}
		>
			{displayName === 'Go back' ? (
				<a href="/">{displayName}</a>
			) : (
				<p>{displayName}</p>
			)}
		</Button>
	)
}

export default PrimaryButton

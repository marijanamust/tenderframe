import React, { FC } from 'react'
import { makeStyles, Theme, Typography, Paper } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: theme.spacing(4),
	},
}))

const NoMatch: FC = () => {
	const classes = useStyles()
	return (
		<div className={classes.root}>
			<Typography component="h1" variant="h3">
				OOOOOOOOOOOOPS
			</Typography>
			<Paper className={classes.root}>
				<Typography variant="h5" component="h3">
					We can't seem to find the page you are looking for.
				</Typography>
				<Typography component="p">Error Code 404</Typography>
			</Paper>
		</div>
	)
}

export default NoMatch

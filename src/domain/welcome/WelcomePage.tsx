import React, { FC } from 'react'
import { makeStyles, Theme, Typography, Paper } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: theme.spacing(4),
	},
}))

const WelcomePage: FC = ({ children }) => {
	const classes = useStyles()
	return (
		<div className={classes.root}>
			<Typography component="h1" variant="h3">
				Marijana's Coding Challenge Trial
			</Typography>
			<Paper className={classes.root}>
				<Typography variant="h5" component="h3">
					Please read the explanation in the email
				</Typography>
				<Typography component="p">
					Thank you for the opportunity, it was an interesting challenge
				</Typography>
			</Paper>
			{children}
		</div>
	)
}

export default WelcomePage

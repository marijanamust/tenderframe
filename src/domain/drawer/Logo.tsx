import React, { FC } from 'react'
import { Link } from 'react-router-dom'

import { makeStyles } from '@material-ui/core'

export interface LogoProps {
	invert?: boolean
	marginTop?: number
	marginBottom?: number
}

const useStyles = makeStyles(() => ({
	logo: {
		padding: '10px',
		width: '100px',
		height: '100px',
		marginTop: '40px',
		marginLeft: '20px',
	},
}))

const Logo: FC<LogoProps> = () => {
	const classes = useStyles()

	return (
		<Link to="/">
			<img src="/tenderframe-192.png" className={classes.logo} alt="Logo" />
		</Link>
	)
}

export default Logo

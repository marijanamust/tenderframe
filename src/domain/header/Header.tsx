/* eslint-disable import/order */
/* eslint-disable import/imports-first */
import React, { FC } from 'react'

import Projects from './Projects'
import { makeStyles, AppBar } from '@material-ui/core'

import { themeValues } from 'app/themes/themeValues'

const useStyles = makeStyles({
	root: {
		height: themeValues().sizes.TabHeader.height,
		background: themeValues().gradients.background,
		position: 'relative',
		color: 'white',
	},
})

const Header: FC = () => {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<Projects />
			<AppBar position="static" />
		</div>
	)
}

export default Header

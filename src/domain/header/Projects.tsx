import React, { FC } from 'react'
import { useQuery } from '@apollo/react-hooks'
import { Link } from 'react-router-dom'
import { gql } from 'apollo-boost'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

const getAllProjects = gql`
	{
		allProjects(first: 3) {
			id
			name
		}
	}
`

interface Project {
	id: string
	name: string
	[index: string]: any
}

const useStyles = makeStyles({
	root: {
		flexGrow: 1,
	},
})

const Projects: FC = () => {
	const classes = useStyles()
	const [value, setValue] = React.useState(0)

	const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
		setValue(newValue)
	}
	const { loading, error, data } = useQuery(getAllProjects)

	if (loading) return <p>Loading...</p>
	if (error) return <p>Error :(</p>

	return (
		<Paper className={classes.root}>
			<Tabs
				value={value}
				onChange={handleChange}
				indicatorColor="primary"
				textColor="primary"
				centered
			>
				{data.allProjects.map((project: Project) => (
					<Tab
						key={project.id}
						label={project.name}
						component={Link}
						to={{ pathname: `/projects/${project.id}` }}
					/>
				))}
			</Tabs>
		</Paper>
	)
}

export default Projects

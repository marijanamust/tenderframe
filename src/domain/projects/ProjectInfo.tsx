import React from 'react'
import { makeStyles, Theme, Typography, Paper } from '@material-ui/core'
import { useParams } from 'react-router-dom'
import { useQuery } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'
import PrimaryButton from '../../component/PrimaryButton'

const GET_PROJECT = gql`
	query project($id: ID) {
		project(id: $id) {
			name
		}
	}
`

// interface IProject {
// 	id: string
// 	name: string
// 	[index: string]: any
// }

interface RouteParams {
	projectId: string
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: theme.spacing(4),
	},
}))

const ProjectInfo = () => {
	const classes = useStyles()
	const params = useParams<RouteParams>()

	const { loading, error, data } = useQuery(GET_PROJECT, {
		variables: { id: params.projectId },
	})

	if (loading) return <p>Loading...</p>
	if (error) return <p>Error :(</p>

	return (
		<div className={classes.root}>
			<Typography component="h1" variant="h3">
				{data.project.name}
			</Typography>
			<Paper className={classes.root}>
				<p>Project ID: {params.projectId}</p>
				<PrimaryButton name="Click" color="red" variant="outlined" />
				<PrimaryButton name="Like" color="blue" variant="text" />
				<PrimaryButton name="Go back" color="green" variant="contained" />
			</Paper>
		</div>
	)
}

export default ProjectInfo
